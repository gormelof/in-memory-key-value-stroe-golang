package io

import (
	"encoding/json"
	"errors"
	config "gitlab.com/gormelof/in-memory-key-value-stroe-golang"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

func Backup(data *map[string]string) {
	for range time.Tick(time.Second * config.BackupSeconds) {
		dataStr, err := json.Marshal(data)
		if err != nil {
			log.Fatal(err)
		}

		// names
		var filename = strconv.FormatInt(time.Now().Unix(), 10) + "-data.json"

		// directory
		if _, err := os.Stat(config.Dirname); errors.Is(err, os.ErrNotExist) {
			os.Mkdir(config.Dirname, 0755)
		}

		// create file
		f, err := os.Create(config.Dirname + "/" + filename)
		if err != nil {
			log.Fatal(err)
		}

		defer f.Close()

		// write
		_, err = f.Write(dataStr)
		if err != nil {
			log.Fatal(err)
		}

		// get old file(s)
		files, err := ioutil.ReadDir(config.Dirname)
		if err != nil {
			log.Fatal(err)
		}

		// remove old file(s)
		for _, file := range files {
			if file.Name() != filename {
				err = os.Remove(config.Dirname + "/" + file.Name())
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}
}

func Sync(data *map[string]string) {
	files, err := ioutil.ReadDir(config.Dirname)
	if err != nil {
		log.Fatal(err)
	}

	if len(files) == 0 {
		return
	}
	file := files[0]

	b, err := ioutil.ReadFile(config.Dirname + "/" + file.Name())
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(b, &data)
	if err != nil {
		log.Fatal(err)
	}
}
