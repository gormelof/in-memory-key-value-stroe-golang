package main

import (
	"encoding/json"
	"gitlab.com/gormelof/in-memory-key-value-stroe-golang/internal/server"
	"io/ioutil"
	"log"
	"net/http"
)

var storage *internal.Storage

func main() {
	storage = internal.NewStorage()
	log.Println("Storage initializing...")
	storage.Init()

	http.HandleFunc("/get", get)
	http.HandleFunc("/set", set)
	http.HandleFunc("/flush", flush)

	log.Println("Server listening on port 8090!")
	http.ListenAndServe(":8090", nil)
}

func get(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		if key := req.URL.Query().Get("key"); len(key) > 0 {
			result := storage.Get(key)
			if len(result.Value) > 0 {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)

				json.NewEncoder(w).Encode(result)
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func set(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		requestBody, err := ioutil.ReadAll(req.Body)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		var kv internal.KV

		json.Unmarshal(requestBody, &kv)

		storage.Set(kv.Key, kv.Value)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func flush(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		storage.Flush()
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
