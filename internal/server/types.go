package internal

type Storage struct {
	data map[string]string
}

type KV struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
