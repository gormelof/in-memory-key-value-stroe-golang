# In Memory Key Value Stroe Golang

- This project is a simple key-value store application.
- Data is created and retrieved with the Rest API.
- It writes to the file at certain time intervals. (Default 60 Seconds)
- Developed with Go using only standard libraries. 

## Run
You can run it with the following command. 

`go run cmd/server/main.go`

## Endpoints

### Set Value

Adds data.

**Post**

> /set

**Body**

|Filed           |Type     |Desc                     |
|----------------|---------|-------------------------|
|`key`	         |String   |key                      |
|`value`         |String   |value                    |

**Success response**

_200 OK_
```json
{}
```

### Get Value

It fetches data. 

**Get**

> /get?key=key1

**Query Params**

|Filed           |Type     |Desc                     |
|----------------|---------|-------------------------|
|`key`	         |String   |key                      |

**Success response**

_200 OK_
```json
{
    "key": "value"
}
```

### Flush Data

It deletes all data.

**Post**

> /flush

**Body**

**Success response**

_200 OK_
```json
{}
```
