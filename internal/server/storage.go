package internal

import (
	"gitlab.com/gormelof/in-memory-key-value-stroe-golang/pkg"
	"sync"
)

var (
	once     sync.Once
	instance *Storage
)

func NewStorage() *Storage {
	once.Do(func() {
		instance = &Storage{data: make(map[string]string)}
	})
	return instance
}

func (s *Storage) Init() {
	io.Sync(&s.data)
	go io.Backup(&s.data)
}

func (s *Storage) Set(key string, value string) {
	if val, ok := s.data[key]; ok {
		s.data[key] = val
	} else {
		s.data[key] = value
	}
}

func (s *Storage) Get(key string) KV {
	result := KV{Key: key}

	if val, ok := s.data[key]; ok {
		result.Value = val
		return result
	}

	return result
}

func (s *Storage) Flush() {
	s.data = map[string]string{}
}
